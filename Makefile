# mandatory vars:

binary_name			:= example
type				:= exec



# optional vars:
include_paths			:=
library_paths			:=

source_extension		?= .c
source_paths			?= src src/civetweb
libs_to_link			:=

main_source_paths		:= src/main

test_source_paths		:= src/test
#test_add_libs_to_link		?=

#output_folder			?= build
#DESTDIR				?=
PREFIX				:= $(CURDIR)/install

format_dirs			:= src
format_extensions		?= .h .c

subcomponent_paths		:=

include lib/generic_makefile/modular_makefile.mk
